import requests
import json


class Http:
    def post(self, url, dic: dict) -> requests.request:
        data = json.dumps(dic)
        r = requests.post(url, data)
        return r
