import configparser
import os
from bsf.util import *


class BsfConfig:
    @classmethod
    def isdebuglog(cls):
        '''Debug日志配置'''
        return cls.getvalue("isdebuglog", "true")

    @classmethod
    def iserrorlog(cls):
        '''Error日志配置'''
        return cls.getvalue("iserrorlog", "true");

    @classmethod
    def iscommonlog(cls):
        '''
        Comm日志配置
        普通日志
        :return:
        '''
        return BsfConfig.getvalue("iscommonlog", "true");

    @classmethod
    def istimewatchlog(cls):
        '''
        耗时日志配置
        普通日志
        :return:
        '''
        return cls.getvalue("istimewatchlog", "true");

    '''当前项目名称'''

    @classmethod
    def projectname(cls):
        return cls.getvalue("projectname", "未命名项目");

    @classmethod
    def projectdeveloper(cls):
        '''当前项目默认开发人员'''
        return cls.getvalue("projectdeveloper", "");

    '''bsf配置文件名'''
    _bsfConfigFileName = "bsf.ini";

    @classmethod
    def getvalue(cls, key, defaultvalue):
        if os.path.exists(cls._bsfConfigFileName):
            cf = configparser.ConfigParser()
            cf.read(cls._bsfConfigFileName);
            value = cf.get("bsf", key)
            return value
        else:
            return defaultvalue


class BsfException(Exception):
    bsfMessage = ''
    bsfExceptionList = []

    def __init__(self, message, clsname=''):
        self.bsfMessage += clsname + StringUtil.nullToEmpty(message)
        super().__init__(message)

    def __init__(self, message, exception, clsname=''):
        if not exception:
            self.bsfMessage += clsname + StringUtil.nullToEmpty(message)
            self.bsfExceptionList.append(exception)
        super().__init__(message, exception)
