
import datetime
from bsf.util import *


class DebugLog:
    @staticmethod
    def writeln(msg, classname=''):
        filepath = os.path.join(os.getcwd(), "debug." + datetime.datetime.now().strftime('%y-%m-%d') + ".log")
        message = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " " + classname + " " + msg+os.linesep
        print("【debug】"+message)
        IoUtil.createdir(filepath)
        IoUtil.append(filepath, message)


class ErrorLog:
    @staticmethod
    def writeln(msg,exception: Exception,classname=''):
        filepath = os.path.join(os.getcwd(), "error." + datetime.datetime.now().strftime('%y-%m-%d') + ".log")
        message = (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " " + classname + " " + msg + os.linesep +
                   "【详细错误】" + repr(exception) +os.linesep)
        print("【error】"+message)
        IoUtil.createdir(filepath)
        IoUtil.append(filepath, message)


class CommonLog:
    @staticmethod
    def writeln(msg, classname=''):
        filepath = os.path.join(os.getcwd(), "common." + datetime.datetime.now().strftime('%y-%m-%d') + ".log")
        message = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " " + classname + " " + msg + os.linesep
        print("【common】" + message)
        IoUtil.createdir(filepath)
        IoUtil.append(filepath, message)


