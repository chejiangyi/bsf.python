import simplejson


class Json:
    def to_json(self, obj):
        if '__dict__' in dir(obj):
            return simplejson.dumps(obj.__dict__)
        else:
            return simplejson.dumps(obj)

    def to_object(self, obj:object, string):
        if '__dict__' in dir(obj):
            obj.__dict__.update(simplejson.loads(string))
        else:
            return simplejson.loads(string)
