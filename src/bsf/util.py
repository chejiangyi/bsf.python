import os
import configparser


class StringUtil:
    @staticmethod
    def nullToEmpty(str):
        if str is None:
            return str
        else:
            return ""


class ConfigUtil:
    @staticmethod
    def getvalue(file_path, category, key, default_value='') -> str:
        if os.path.exists(file_path):
            cf = configparser.ConfigParser()
            cf.read(file_path);
            value = cf.get(category, key)
            if value is None:
                return default_value
            else:
                return value
        else:
            return default_value


class IoUtil:

    @staticmethod
    def createdir(file_path):
        if os.path.exists(file_path):
            return
        if os.path.isfile(file_path):
            dir = os.path.dirname(file_path)
            if not os.path.exists(dir):
                os.path.makedirs(file_path)
            os.path.mknod(file_path)
            return
        if os.path.isdir(file_path):
            os.path.makedirs(file_path)

    @staticmethod
    def append(file_path, msg):
        with open(file_path, 'a') as f:
            f.write(msg+os.linesep)
